using System;
using Alfresco.Authentication.Client;
using System.Net;
using Alfresco.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Web;

namespace Alfresco.Core.Api
{
    public class NodesApi
    {
        public IFormFile MyFile { set; get; }
        private static JObject json;

        public NodesApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
        public ApiClient ApiClient { get; set; }

        /// Create a node
        /// Create a node and add it as a primary child of node **nodeId**. 
        /// <param name="nodeId"> You can also use one of these well-known aliases:
        /// -my- * -shared- * -root-
        public void CreateNode(IFormFile file,  string nodeId)
        {      
            Console.WriteLine("===============================================");
            Console.WriteLine("Create ticket...");

            var authClient = new Alfresco.Authentication.Client.ApiClient(Constants.authBase);
            var authApi = new Alfresco.Authentication.Api.AuthenticationApi(authClient);
            var ticket = authApi.CreateTicket(Constants.ticketBodyCreate);

            if (ticket == null) return;
            var path = $"/nodes/{nodeId}/children?alf_ticket=";

            using (var client = new HttpClient())
            {
                // Get all files from Alfresco.upload directory
                // string[] allfiles = Directory.GetFiles("./Alfresco.upload/", "*.*", SearchOption.AllDirectories);
            
                // foreach (var file in files)
                // {
                    using (var formData = new MultipartFormDataContent())
                    {
                        // **filedata** is field to represent the content to upload
                        formData.Add(new StreamContent(file.OpenReadStream()), "filedata", file.Name);

                        // overwrite file or not
                        formData.Add(new StringContent("false"), "overwrite");
                            // formData.Add(new StreamContent(File.Open(s, FileMode.Open, FileAccess.ReadWrite)), "filedata", name.Name);
                        try
                        {
                            var response = client.PostAsync($"{Constants.coreBase}" + path + ticket.Entry.Id, formData).Result;
                            Console.WriteLine("Success! File uploaded!");
                        }
                        catch (HttpRequestException requestException)
                        {
                            throw new ApiException("Error calling CreateNode: " + requestException.Message);

                        }
                }
            }
        }

        /// Get node content 
        /// <param name="nodeId">The identifier of a node.</param> 
        public HttpWebResponse GetNodeContent(string nodeId)
        {
            Console.WriteLine("===============================================");
            Console.WriteLine("Create ticket...");

            var authClient = new Alfresco.Authentication.Client.ApiClient(Constants.authBase);
            var authApi = new Alfresco.Authentication.Api.AuthenticationApi(authClient);
            var ticket = authApi.CreateTicket(Constants.ticketBodyCreate);
            
            // if (ticket == null) return;
            var path = $"/nodes/{nodeId}/content?alf_ticket=";

            var nodeInfo = $"/nodes/{nodeId}?alf_ticket=";
            // get file info
            var httpRequestNodeInfo = (HttpWebRequest)WebRequest.Create($"{Constants.coreBase}" + nodeInfo + ticket.Entry.Id);
            var httpResponseNodeInfo = (HttpWebResponse)httpRequestNodeInfo.GetResponse();

            using (var streamReader = new StreamReader(httpResponseNodeInfo.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                json = JObject.Parse(result);

                // get file name
                var saveTo = $"./Alfresco.download/{json["entry"]["name"]}";
                var httpRequestGetNode = (HttpWebRequest)WebRequest.Create($"{Constants.coreBase}" + path + ticket.Entry.Id);
                var httpResponseGetNode = (HttpWebResponse)httpRequestGetNode.GetResponse();
                
                return httpResponseGetNode;
                // CopyStream(httpResponseGetNode.GetResponseStream(), saveTo);
            }
        }

        public void CopyStream(Stream stream, string destPath)
        {
            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                stream.CopyTo(fileStream);
            }
        }
    }
}