using System;
using RestSharp;
using Newtonsoft.Json;

namespace Alfresco.Authentication.Client
{
    public class ApiClient
    {
        private readonly Dictionary<String, String> _defaultHeaderMap = new Dictionary<String, String>();

        public ApiClient(String basePath="https://10.15.16.146:8080//alfresco/api/-default-/public/authentication/versions/1")
        {
            BasePath = basePath;
            RestClient = new RestClient(BasePath);
        }

        public string BasePath { get; set; }
        public RestClient RestClient { get; set; }

        public object Deserialize(string content, Type type)
        {
            Console.WriteLine("______________");
            return JsonConvert.DeserializeObject(content, type);

        }
         public string Serialize(object obj)
        {
            return obj != null ? JsonConvert.SerializeObject(obj) : null;  
        }   
    }
}