using System;
using Newtonsoft.Json;
public class Ticket {

    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }
    public int val {get;set;}

    [JsonProperty(PropertyName = "userId")]
    public string UserId { get; set; }

}