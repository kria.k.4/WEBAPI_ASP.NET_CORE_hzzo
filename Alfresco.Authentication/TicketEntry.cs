using System;
using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Alfresco.Authentication
{
    public class TicketEntry
    {

        [JsonProperty(PropertyName = "entry")]
        public Ticket Entry { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class TicketEntry {\n");
            sb.Append("  Entry: ").Append(Entry).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}