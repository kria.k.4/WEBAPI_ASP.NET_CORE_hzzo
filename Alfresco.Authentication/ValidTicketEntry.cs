using System;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Runtime.Serialization;

namespace Alfresco.Authentication
{
    public class ValidTicketEntry
    {
        [JsonProperty(PropertyName = "entry")]
        public ValidTicket Entry { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ValidTicketEntry {\n");
            sb.Append("  Entry: ").Append(Entry).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}