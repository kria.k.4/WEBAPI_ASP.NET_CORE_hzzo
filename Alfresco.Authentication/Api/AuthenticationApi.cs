using Microsoft.VisualBasic;
using System;
using System.Net;
using Alfresco.Authentication.Client;
using Alfresco.Core.Api;
using Alfresco.Exceptions;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net.NetworkInformation;

namespace Alfresco.Authentication.Api
{
    public class AuthenticationApi
    {
        bool vpn = false;
        private static JObject json;
        public static ApiClient DefaultApiClient = new ApiClient();

        public AuthenticationApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = DefaultApiClient;
            else
                this.ApiClient = apiClient;
        }

        public ApiClient ApiClient { get; set; }

        /// Create ticket (login) 
        /// Logs in and returns the new authentication ticket 
        /// The userId and password properties are mandatory in the request body 
        /// To use the ticket in future requests you should pass it in the request header
        /// <param name="ticketBodyCreate">The user credential.</param> 

        public TicketEntry CreateTicket(String ticketBodyCreate)
        {
                Console.WriteLine("===============================================");
                Console.WriteLine("Logging into Alfresco...");

                var path = "/tickets";
                var url = Constants.authBase + path;

                var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                httpRequest.Method = "POST";

                httpRequest.Headers["Authorization"] = "Basic";
                httpRequest.ContentType = "application/json; charset=utf-8";
                httpRequest.Accept = "application/json";

                using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    streamWriter.Write(ticketBodyCreate);
                }

                try
                {
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    var result = "";
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = streamReader.ReadToEnd();
                        json = JObject.Parse(result);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                return (TicketEntry)ApiClient.Deserialize(json.ToString(), typeof(TicketEntry));
        }

        /// Validate ticket 
        /// Validates the specified ticket is still valid.
        public ValidTicketEntry ValidateTicket(String ticket)
        {
            var path = "/tickets/-me-?alf_ticket=" + ticket;
            var url = Constants.authBase + path;

            var httpRequest = (HttpWebRequest)WebRequest.Create(url);
            httpRequest.Method = "GET";

            httpRequest.Headers["Authorization"] = "Basic";
            httpRequest.ContentType = "application/json; charset=utf-8";
            httpRequest.Accept = "application/json";

            try
            {
                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    json = JObject.Parse(result);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return (ValidTicketEntry)ApiClient.Deserialize(json.ToString(), typeof(ValidTicketEntry));
        }
    }
}