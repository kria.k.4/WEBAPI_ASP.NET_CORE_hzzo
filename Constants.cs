using System;

static class Constants
{
    public const string authBase = "http://10.15.16.146:8080/alfresco/api/-default-/public/authentication/versions/1";
    public const string coreBase = "http://10.15.16.146:8080/alfresco/api/-default-/public/alfresco/versions/1";

    // save somewhere else
    public const string ticketBodyCreate = @"{
            ""userId"": ""admin"",
            ""password"": ""hyyo_secret""
        }";
}