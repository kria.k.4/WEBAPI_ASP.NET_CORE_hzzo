using System.Net; 
using Microsoft.VisualBasic; 
using System; 
using Alfresco.Authentication.Api; 
using Alfresco.Authentication.Client; 
using Alfresco.Core.Api; 
using System.Net.NetworkInformation; 
var builder = WebApplication.CreateBuilder(args); 
 
// Add services to the container. 
 
builder.Services.AddControllers(); 
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle 
builder.Services.AddEndpointsApiExplorer(); 
builder.Services.AddSwaggerGen(); 
builder.Services.AddHttpClient(); 
 
var app = builder.Build(); 
 
// Configure the HTTP request pipeline. 
if (app.Environment.IsDevelopment()) 
{ 
    app.UseSwagger(); 
    app.UseSwaggerUI(); 
} 
 
app.UseHttpsRedirection(); 
 
app.UseAuthorization(); 
 
app.MapControllers(); 
 
app.MapGet("/", () => "Hello World! Welcome to Web API!"); 
 
var authClient = new Alfresco.Authentication.Client.ApiClient(Constants.authBase); 
var authApi = new Alfresco.Authentication.Api.AuthenticationApi(authClient); 
var nodesApi = new Alfresco.Core.Api.NodesApi(Constants.coreBase); 
 
app.MapPost("/CreateNode", async (HttpRequest request) => 
    { 
        { 
            IFormFile file = request.Form.Files[0]; 
            nodesApi.CreateNode(file, "-root-"); 
        } 
        return Results.Ok("OK!"); 
    }); 
 
app.MapGet("/GetNodeContent", async (string nodeId) => 
{ 
    HttpWebResponse nodeContent = nodesApi.GetNodeContent(nodeId); 
     
    return nodeContent; 
}); 
 
app.Run(); 
