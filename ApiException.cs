using System;

namespace Alfresco.Exceptions {

  public class ApiException : Exception {

      public int ErrorCode { get; set; }
      public Object ErrorContent { get; private set; }
      public ApiException() {}


      public ApiException(string message) : base(message) {
      }
      public ApiException(int errorCode, string message, Object errorContent = null) : base(message) {
          this.ErrorCode = errorCode;
          this.ErrorContent = errorContent;
      }

  }

}
